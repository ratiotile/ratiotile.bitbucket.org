var gradebook = priv.stats_gradebook;
var gradebook_keys = _.keys(gradebook);
var prelabs = [ _.find(gradebook_keys, function(k){
                  return k.indexOf("Mean versus Median Prelab") > -1;
                }), 
                _.find(gradebook_keys, function(k){
                  return k.indexOf("Histograms and Boxplots Prelab") > -1;
                })
              ],
    prelabScore = 0,
    prelabMsg;

console.log("prelabs:",prelabs);


if(gradebook){
  var prelabData = _.pick(gradebook, prelabs);
  console.log("prelabData");
  _.each(prelabData, function(prelab){
    if(prelab && parseInt(prelab) > 0){
      ++prelabScore;
    }
  });
}

console.log(prelabScore, prelabs.length, prelabs );
switch(prelabScore){
  case prelabs.length: // got at least 1 point on each prelab
    prelabMsg = "Good job doing the first two prelabs!";
    break;
  default: // missed at least one prelab
    prelabMsg = "According to our records you have missed at least one prelab. " +
                "You may not think that these few points will make a difference, " +
                "but missing multiple prelabs can add up!";
}

$('#prelabmsg').text(prelabMsg);